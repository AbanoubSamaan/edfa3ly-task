<?php

namespace App\Constants;

use Illuminate\Http\Response;

/**
 * This class contains error codes returned by the api with their respective messages 
 * and http response codes
 *
 * @author Abanoub Samaan
 */
class Error {

    const UNKNOWN_ERROR = 1;
    const PRODUCT_NOT_FOUND = 2;
    const CART_NOT_FOUND = 3;
    const PRODUCT_DOES_NOT_IN_CART = 4;
    const OFFER_NOT_FOUND = 5;
    
    const MSG = array(
        self::UNKNOWN_ERROR => 'Unkown error',
        self::PRODUCT_NOT_FOUND => 'Product not found',
        self::CART_NOT_FOUND => 'Cart not found',
        self::PRODUCT_DOES_NOT_IN_CART => 'Product doesn\'t exsit in this cart',
        self::OFFER_NOT_FOUND => 'Offer not found',
    );
    
    const HTTPCODE = array(
        self::UNKNOWN_ERROR => Response::HTTP_INTERNAL_SERVER_ERROR,
        self::CART_NOT_FOUND => Response::HTTP_NOT_FOUND,
        self::PRODUCT_NOT_FOUND => Response::HTTP_NOT_FOUND,
        self::PRODUCT_DOES_NOT_IN_CART => Response::HTTP_BAD_REQUEST,
        self::OFFER_NOT_FOUND => Response::HTTP_NOT_FOUND,
    );

}
