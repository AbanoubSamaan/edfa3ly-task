<?php

/**
 * Eloquent model for the cart table
 *
 * @author Abanoub Samaan
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Cart extends Model {

    protected $table = 'cart';
    protected $hidden = [
        'created_at', 'updated_at'
    ];

    /**
     *
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = [
        "created_at",
        "updated_at",
    ];

    /**
     * Get related products
     *
     * @return Illuminate\Database\Eloquent\Relations\Relation
     */
    public function products() {
        return $this->hasMany('App\Models\CartProduct');
    }

    /**
     * Get related user
     *
     * @return Illuminate\Database\Eloquent\Relations\Relation
     */
    public function user() {
        return $this->belongsTo('App\Models\User');
    }

    /**
     * Get related offers applied on the cart
     *
     * @return Illuminate\Database\Eloquent\Relations\Relation
     */
    public function appliedOffers() {
        return $this->hasManyThrough('App\Models\AppliedOffer', 'App\Models\CartProduct');
    }

}
