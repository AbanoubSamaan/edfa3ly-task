<?php

/**
 * Eloquent model for the product table
 *
 * @author Abanoub Samaan
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Product extends Model {

    protected $table = 'product';
    protected $hidden = [
        'created_at', 'updated_at'
    ];

    /**
     *
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = [
        "created_at",
        "updated_at",
    ];

    /**
     * Get related offers
     *
     * @return Illuminate\Database\Eloquent\Relations\Relation
     */
    public function offers() {
        return $this->hasMany('App\Models\Offer', 'buy_product_id');
    }

}
