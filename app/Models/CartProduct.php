<?php

/**
 * Eloquent model for the cart product table
 *
 * @author Abanoub Samaan
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CartProduct extends Model {

    protected $table = 'cart_product';
    protected $hidden = [
        'id', 'product_id', 'cart_id', 'created_at', 'updated_at'
    ];

    /**
     *
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = [
        "created_at",
        "updated_at",
    ];

    /**
     * Get related product
     *
     * @return Illuminate\Database\Eloquent\Relations\Relation
     */
    public function product() {
        return $this->belongsTo('App\Models\Product');
    }

    /**
     * Get related cart
     *
     * @return Illuminate\Database\Eloquent\Relations\Relation
     */
    public function cart() {
        return $this->belongsTo('App\Models\Cart');
    }
    
    /**
     * Get related cart
     *
     * @return Illuminate\Database\Eloquent\Relations\Relation
     */
    public function appliedOffer() {
        return $this->hasOne('App\Models\AppliedOffer');
    }

}
