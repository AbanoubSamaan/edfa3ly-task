<?php

/**
 * Eloquent model for the cart applied offers on product table
 *
 * @author Abanoub Samaan
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AppliedOffer extends Model {

    protected $table = 'applied_offer';
    protected $hidden = [
        'id', 'cart_product_id', 'offer_id', 'created_at', 'updated_at'
    ];

    /**
     *
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = [
        "created_at",
        "updated_at",
    ];

    /**
     * Get related Offer
     *
     * @return Illuminate\Database\Eloquent\Relations\Relation
     */
    public function offer() {
        return $this->belongsTo('App\Models\Offer');
    }

}
