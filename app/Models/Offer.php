<?php

/**
 * Eloquent model for the offer table
 *
 * @author Abanoub Samaan
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Offer extends Model {

    protected $table = 'offer';
    protected $hidden = [
        'created_at', 'updated_at', 'buy_product_id', 'minimum_quantity', 'get_product_id',
    ];

    /**
     *
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = [
        "created_at",
        "updated_at",
    ];

    /**
     * Get related product
     *
     * @return Illuminate\Database\Eloquent\Relations\Relation
     */
    public function buyProduct() {
        return $this->belongsTo('App\Models\Product', 'buy_product_id');
    }

    /**
     * Get related product
     *
     * @return Illuminate\Database\Eloquent\Relations\Relation
     */
    public function getProduct() {
        return $this->belongsTo('App\Models\Product', 'get_product_id');
    }

}
