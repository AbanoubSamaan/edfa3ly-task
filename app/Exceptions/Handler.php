<?php

namespace App\Exceptions;

use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use App\Services\GeneralService;
use App\Constants\Error;

class Handler extends ExceptionHandler {

    /**
     * A list of the exception types that are not reported.
     *
     * @var array
     */
    protected $dontReport = [
            //
    ];

    /**
     * A list of the inputs that are never flashed for validation exceptions.
     *
     * @var array
     */
    protected $dontFlash = [
        'password',
        'password_confirmation',
    ];

    /**
     * Register the exception handling callbacks for the application.
     *
     * @return void
     */
    public function register() {
        //
    }

    /**
     * Render an exception into an HTTP response.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Throwable  $exception
     * 
     * @return \Illuminate\Http\Response
     */
    public function render($request, \Throwable $exception) {
        $generalService = new GeneralService();
        if (is_a($exception, 'App\Exceptions\GeneralException')) {
            return $generalService->getErrorResponse(array($exception->getCode()), $exception->getHttpCode());
        }
        $cousedBy = array("msg" => $exception->getMessage(), "code" => $exception->getCode(), "file" => $exception->getFile(), "line" => $exception->getLine());
        return $generalService->getErrorResponse(array(Error::UNKNOWN_ERROR), Error::HTTPCODE[Error::UNKNOWN_ERROR], $cousedBy);
//         return parent::render($request, $exception);
    }

}
