<?php

/**
 * This exception is thrown in case of cart access errors
 *
 * @author Abanoub Samaan
 */

namespace App\Exceptions;

class CartException extends GeneralException {

    public function __construct($code) {
        parent::__construct($code);
    }

}
