<?php

/**
 * This exception is thrown in case of product access errors
 *
 * @author Abanoub Samaan
 */

namespace App\Exceptions;

class ProductException extends GeneralException {

    public function __construct($code) {
        parent::__construct($code);
    }

}
