<?php

/**
 * This exception is inherited by all maillabs exceptions
 *
 * @author Abanoub Samaan
 */

namespace App\Exceptions;

class GeneralException extends \Exception {

    public function __construct($code) {
        $this->httpCode = \App\Constants\Error::HTTPCODE[$code];
        parent::__construct(\App\Constants\Error::MSG[$code], $code);
    }

    private $httpCode;

    public function getHttpCode() {
        return $this->httpCode;
    }

}
