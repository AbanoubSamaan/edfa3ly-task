<?php

/**
 * This exception is thrown in case of offer access errors
 *
 * @author Abanoub Samaan
 */

namespace App\Exceptions;

class OfferException extends GeneralException {

    public function __construct($code) {
        parent::__construct($code);
    }

}
