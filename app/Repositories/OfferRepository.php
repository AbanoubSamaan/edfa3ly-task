<?php

/**
 * The repository for offers
 *
 * @author Abanoub Samaan
 */

namespace App\Repositories;

use App\Models\Offer;

class OfferRepository {

    /**
     * getOfferById
     *
     * @param  int $offerId
     *
     * @return App\Models\Offer
     */
    public function getOffer($offerId) {
        return Offer::where('id', $offerId)
                        ->with('buyProduct', 'getProduct')
                        ->first();
    }

    /**
     * Check if Offer exists by it's id
     *
     * @param int $offerId
     * @return boolean
     */
    public function offerExists($offerId) {
        return Offer::where('id', $offerId)
                        ->exists();
    }

    /**
     * Gets a collection of all available offers
     *
     * @return Illuminate\Support\Collection of App\Models\Offer
     */
    public function getOffers() {
        $offers = Offer::with('buyProduct', 'getProduct')
                ->paginate(\Config::get('general-config.recordsPerPage'));
        return $offers;
    }

}
