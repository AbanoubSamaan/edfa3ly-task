<?php

/**
 * The repository for cart product
 *
 * @author Abanoub Samaan
 */

namespace App\Repositories;

use App\Models\CartProduct;

class CartProductRepository {

    /**
     * Get cart product by cart_id and product_id
     *
     * @param int $cartId
     * @param int $productId
     * 
     * @return CartProduct
     */
    public function getCartProductByCartIdAndProductId($cartId, $productId) {
        return CartProduct::where('cart_id', $cartId)->where('product_id', $productId)
                        ->first();
    }

    /**
     * Check if cart product exists by cart_id and product_id
     *
     * @param int $cartId
     * @param int $productId
     * 
     * @return boolean
     */
    public function cartProductExists($cartId, $productId) {
        return CartProduct::where('cart_id', $cartId)->where('product_id', $productId)
                        ->exists();
    }

    /**
     * Add cart product quantity
     *
     * @param int $cartId
     * @param int $productId
     * @param int $quantity
     * @param float $total
     * 
     * @return boolean
     */
    public function addCartProduct($cartId, $productId, $quantity, $total) {
        $cartProduct = new CartProduct();
        $cartProduct->cart_id = $cartId;
        $cartProduct->product_id = $productId;
        $cartProduct->quantity = $quantity;
        $cartProduct->total = $total;
        $cartProduct->save();
        return $cartProduct;
    }

    /**
     * Delete cart product
     *
     * @param int $cartId
     * @param int $productId
     * 
     * @return boolean
     */
    public function deleteCartProduct($cartId, $productId) {
        return CartProduct::where('cart_id', $cartId)->where('product_id', $productId)
                        ->delete();
    }

    /**
     * Update cart product quantity
     *
     * @param int $cartId
     * @param int $productId
     * @param int $quantity
     * @param float $total
     * 
     * @return boolean
     */
    public function updateCartProduct($cartId, $productId, $quantity, $total) {
        $updatedValues = array(
            'quantity' => $quantity,
            'total' => $total
        );

        return CartProduct::where('cart_id', $cartId)->where('product_id', $productId)
                        ->update($updatedValues);
    }

}
