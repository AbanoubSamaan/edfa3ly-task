<?php

/**
 * The repository for applied offers
 *
 * @author Abanoub Samaan
 */

namespace App\Repositories;

use App\Models\AppliedOffer;

class AppliedOfferRepository {

    /**
     * Add applied offer
     *
     * @param int $cartProductId
     * @param int $offerId
     * @param int $quantity
     * @param float $totalDiscount
     * 
     * @return boolean
     */
    public function addAppliedOffer($cartProductId, $offerId, $quantity, $totalDiscount) {
        $appliedOffer = new AppliedOffer();
        $appliedOffer->cart_product_id = $cartProductId;
        $appliedOffer->offer_id = $offerId;
        $appliedOffer->quantity = $quantity;
        $appliedOffer->total_discount = $totalDiscount;
        $appliedOffer->save();
        return $appliedOffer;
    }

    /**
     * Update applied offer
     *
     * @param int $appliedOfferId
     * @param int $quantity
     * @param float $totalDiscount
     * 
     * @return boolean
     */
    public function updateAppliedOffer($appliedOfferId, $quantity, $totalDiscount) {
        $updatedValues = array(
            'quantity' => $quantity,
            'total_discount' => $totalDiscount
        );

        return AppliedOffer::where('id', $appliedOfferId)
                        ->update($updatedValues);
    }

    /**
     * Delete applied offer
     *
     * @param int $appliedOfferId
     * 
     * @return boolean
     */
    public function deleteAppliedOffer($appliedOfferId) {
        return AppliedOffer::where('id', $appliedOfferId)
                        ->delete();
    }

}
