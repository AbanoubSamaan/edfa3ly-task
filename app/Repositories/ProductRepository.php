<?php

/**
 * The repository for products
 *
 * @author Abanoub Samaan
 */

namespace App\Repositories;

use App\Models\Product;

class ProductRepository {

    /**
     * getProductById
     *
     * @param  int $productId
     *
     * @return App\Models\Product
     */
    public function getProduct($productId) {
        return Product::where('id', $productId)
                        ->with('offers.getProduct')
                        ->first();
    }

    /**
     * Check if product exists by it's id
     *
     * @param int $productId
     * @return boolean
     */
    public function productExists($productId) {
        return Product::where('id', $productId)
                        ->exists();
    }

    /**
     * Gets a collection of all available products
     *
     * @return Illuminate\Support\Collection of App\Models\Product
     */
    public function getProducts() {
        $products = Product::with('offers.getProduct')
                ->paginate(\Config::get('general-config.recordsPerPage'));
        return $products;
    }

}
