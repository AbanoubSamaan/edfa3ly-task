<?php

/**
 * The repository for carts
 *
 * @author Abanoub Samaan
 */

namespace App\Repositories;

use App\Models\Cart;

class CartRepository {

    /**
     * getCart
     *
     * @param  int $cartId
     *
     * @return App\Models\Cart
     */
    public function getCart($cartId) {
        return Cart::where('id', $cartId)
                        ->with('products.product', 'appliedOffers.offer')
                        ->first();
    }

    /**
     * Check if cart exists by it's id
     *
     * @param int $cartId
     * @return boolean
     */
    public function cartExists($cartId) {
        return Cart::where('id', $cartId)
                        ->exists();
    }

    /**
     * Update cart
     *
     * @param int $cartId
     * @param float $subTotal
     * @param float $taxesPercentage
     * @param float $taxes
     * @param float $total
     * 
     * @return boolean
     */
    public function updateCart($cartId, $subTotal, $taxesPercentage, $taxes, $total) {
        $updatedValues = array(
            'subtotal' => $subTotal,
            'taxes_percentage' => $taxesPercentage,
            'taxes' => $taxes,
            'total' => $total
        );

        return Cart::where('id', $cartId)
                        ->update($updatedValues);
    }

}
