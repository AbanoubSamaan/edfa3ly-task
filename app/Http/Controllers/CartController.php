<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Http\Request;
use App\Services\GeneralService;
use App\Services\CartService;

class CartController extends BaseController {

    use AuthorizesRequests,
        DispatchesJobs,
        ValidatesRequests;

    /**
     * Constructor
     *
     * @param GeneralService $generalService
     * @param CartService $cartService
     */
    public function __construct(GeneralService $generalService, CartService $cartService) {
        $this->generalService = $generalService;
        $this->cartService = $cartService;
    }

    private $generalService;
    private $cartService;

    /**
     * Gets a single cart
     * 
     * @param Request $request
     * @param int $cartId
     *  
     * @return Illuminate\Http\Response
     */
    public function getcart(Request $request, $cartId) {
        $cart = $this->cartService->getcart($cartId);
        return $this->generalService->getSuccessResponse($cart);
    }

    /**
     * Adds a product to cart
     *
     * 
     * @param Request $request
     * @param int $cartId
     * 
     * @return Illuminate\Http\Response
     */
    public function addProduct(Request $request, $cartId) {
        $cart = $this->cartService->addProduct($cartId, $request->input('product_id'));
        return $this->generalService->getSuccessResponse($cart);
    }

    /**
     * Removes a product from cart
     *
     * @param Request $request
     * @param int $cartId
     * 
     * @return Illuminate\Http\Response
     */
    public function removeProduct(Request $request, $cartId) {
        $cart = $this->cartService->removeProduct($cartId, $request->input('product_id'));
        return $this->generalService->getSuccessResponse($cart);
    }

}
