<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Http\Request;
use App\Services\GeneralService;
use App\Services\ProductService;

class ProductController extends BaseController {

    use AuthorizesRequests,
        DispatchesJobs,
        ValidatesRequests;

    /**
     * Constructor
     *
     * @param GeneralService $generalService
     * @param ProductService $productService
     */
    public function __construct(GeneralService $generalService, ProductService $productService) {
        $this->generalService = $generalService;
        $this->productService = $productService;
    }

    private $generalService;
    private $productService;

    /**
     * Gets all products
     *
     * @return Illuminate\Http\Response
     */
    public function getProducts(Request $request) {
        $products = $this->productService->getProducts();
        return $this->generalService->getSuccessResponse($products);
    }
    
    /**
     * Gets a single product
     *
     * @return Illuminate\Http\Response
     */
    public function getProduct(Request $request, $productId) {
        $product = $this->productService->getProduct($productId);
        return $this->generalService->getSuccessResponse($product);
    }

}
