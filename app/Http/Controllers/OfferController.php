<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Http\Request;
use App\Services\GeneralService;
use App\Services\OfferService;

class OfferController extends BaseController {

    use AuthorizesRequests,
        DispatchesJobs,
        ValidatesRequests;

    /**
     * Constructor
     *
     * @param GeneralService $generalService
     * @param OfferService $offerService
     */
    public function __construct(GeneralService $generalService, OfferService $offerService) {
        $this->generalService = $generalService;
        $this->offerService = $offerService;
    }

    private $generalService;
    private $offerService;

    /**
     * Gets all offers
     *
     * @return Illuminate\Http\Response
     */
    public function getOffers(Request $request) {
        $offers = $this->offerService->getOffers();
        return $this->generalService->getSuccessResponse($offers);
    }
    
    /**
     * Gets a single offer
     *
     * @return Illuminate\Http\Response
     */
    public function getOffer(Request $request, $offerId) {
        $offer = $this->offerService->getOffer($offerId);
        return $this->generalService->getSuccessResponse($offer);
    }

}
