<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;

use Illuminate\Http\Request;
use App\Services\GeneralService;

class ApiController extends BaseController {

    use AuthorizesRequests,
        DispatchesJobs,
        ValidatesRequests;

    /**
     * Constructor
     *
     * @param GeneralService $generalService
     */
    public function __construct(GeneralService $generalService) {
        $this->generalService = $generalService;
    }

    private $generalService;

    /**
     * Gets the API information
     *
     * @return Illuminate\Http\Response
     */
    public function getApiInformation(Request $request) {
        return $this->generalService->getSuccessResponse(true);
    }

}
