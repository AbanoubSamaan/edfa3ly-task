<?php

/**
 * Business logic for all cart product related operations
 *
 * @author Abanoub Samaan
 */

namespace App\Services;

use App\Constants\Error;
use App\Exceptions\CartException;
use App\Repositories\CartProductRepository;
use App\Services\ProductService;
use App\Services\AppliedOfferService;

class CartProductService {

    /**
     * Constructor
     *
     * @param CartProductRepository $cartProductRepository
     * @param ProductService $productService
     * @param AppliedOfferService $appliedOfferService
     * 
     */
    public function __construct(CartProductRepository $cartProductRepository, ProductService $productService, AppliedOfferService $appliedOfferService) {
        $this->cartProductRepository = $cartProductRepository;
        $this->productService = $productService;
        $this->appliedOfferService = $appliedOfferService;
    }

    /** @var CartProductRepository $cartProductRepository */
    private $cartProductRepository;

    /** @var CartProductRepository $cartProductRepository */
    private $productService;

    /** @var AppliedOfferService $appliedOfferService */
    private $appliedOfferService;

    /**
     * Adds a product to cart
     *
     * 
     * @param int $cartId
     * @param int $productId
     * 
     * @return boolean
     */
    public function addProduct($cartId, $productId) {
        $product = $this->productService->getProduct($productId);
        $cartProduct = $this->cartProductRepository->getCartProductByCartIdAndProductId($cartId, $productId);
        if (is_null($cartProduct)) {
            //Product doesn't exist in the cart => increase the product quantity in the cart
            $this->cartProductRepository->addCartProduct($cartId, $productId, 1, $product->price);
        } else {
            //Product already exists in the cart => Add product to cart
            $newQuantity = $cartProduct->quantity + 1;
            $total = $newQuantity * $product->price;
            $this->cartProductRepository->updateCartProduct($cartId, $productId, $newQuantity, $total);
        }
        return true;
    }

    /**
     * Removes a product from cart
     *
     * @param int $cartId
     * @param int $productId
     * 
     * @return Illuminate\Http\Response
     */
    public function removeProduct($cartId, $productId) {
        $product = $this->productService->getProduct($productId);
        $cartProduct = $this->cartProductRepository->getCartProductByCartIdAndProductId($cartId, $productId);
        if (is_null($cartProduct)) {
            //Product doesn't exist in the cart
            throw new CartException(Error::PRODUCT_DOES_NOT_IN_CART);
        } else {
            //Product already exists in the cart => decrease the product quantity in the cart or remove the product
            if ($cartProduct->quantity > 1) {
                $newQuantity = $cartProduct->quantity - 1;
                $total = $newQuantity * $product->price;
                $this->cartProductRepository->updateCartProduct($cartId, $productId, $newQuantity, $total);
            } else {
                $this->cartProductRepository->deleteCartProduct($cartId, $productId);
            }
        }
    }

    /**
     * Update cart product quantity
     *
     * @param int $cartId
     * @param int $productId
     * @param int $appliedOfferCount
     * @param float $discountPercentage
     * 
     * @return boolean
     */
    public function updateCartProduct($cartId, $productId, $offerId, $appliedOfferCount, $discountPercentage) {
        $product = $this->productService->getProduct($productId);
        $cartProduct = $this->cartProductRepository->getCartProductByCartIdAndProductId($cartId, $productId);
        $totalDiscount = $appliedOfferCount * ($discountPercentage / 100) * $product->price;
        $total = ($cartProduct->quantity * $product->price) - $totalDiscount;
        $this->cartProductRepository->updateCartProduct($cartId, $productId, $cartProduct->quantity, $total);
        if (is_null($cartProduct->appliedOffer)) {
            $this->appliedOfferService->addAppliedOffer($cartProduct->id, $offerId, $appliedOfferCount, $totalDiscount);
        } else {
            $this->appliedOfferService->updateAppliedOffer($cartProduct->appliedOffer->id, $appliedOfferCount, $totalDiscount);
        }
    }

    /**
     * Remove applied offer
     *
     * @param int $cartId
     * @param int $productId
     * @param int $appliedOfferId
     * 
     * @return boolean
     */
    public function removeOffer($cartId, $productId, $appliedOfferId) {
        $product = $this->productService->getProduct($productId);
        $cartProduct = $this->cartProductRepository->getCartProductByCartIdAndProductId($cartId, $productId);
        $total = $cartProduct->quantity * $product->price;
        $this->cartProductRepository->updateCartProduct($cartId, $productId, $cartProduct->quantity, $total);
        $this->appliedOfferService->deleteAppliedOffer($appliedOfferId);
    }

}
