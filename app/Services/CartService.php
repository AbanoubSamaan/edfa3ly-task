<?php

/**
 * Business logic for all cart related operations
 *
 * @author Abanoub Samaan
 */

namespace App\Services;

use App\Constants\Error;
use App\Exceptions\CartException;
use App\Repositories\CartRepository;
use App\Services\ProductService;
use App\Services\CartProductService;

class CartService {

    /**
     * Constructor
     *
     * @param CartRepository $cartRepository
     * @param ProductService $productService
     * @param CartProductService $cartProductService
     * 
     */
    public function __construct(CartRepository $cartRepository, ProductService $productService, CartProductService $cartProductService) {
        $this->cartRepository = $cartRepository;
        $this->productService = $productService;
        $this->cartProductService = $cartProductService;
    }

    /** @var CartRepository $cartRepository */
    private $cartRepository;

    /** @var ProductService $productService */
    private $productService;

    /** @var CartProductService $cartProductService */
    private $cartProductService;

    /**
     * Gets a specific cart by it's id
     *
     * @param int $cartId
     * 
     * @return Cart
     * @throws CartException
     */
    public function getCart($cartId) {
        $cart = $this->cartRepository->getCart($cartId);
        if (is_null($cart)) {
            throw new CartException(Error::CART_NOT_FOUND);
        }
        return $cart;
    }

    /**
     * Check the existance of the given cart id, or throw an exception.
     *
     * @param integer $cartId
     * @return true
     * 
     * @throws CartException
     */
    private function cartExists($cartId) {
        if (!$this->cartRepository->cartExists($cartId)) {
            throw new CartException(Error::CART_NOT_FOUND);
        }
        return true;
    }

    /**
     * Adds a product to cart
     *
     * 
     * @param int $cartId
     * @param int $productId
     * 
     * @throws CartException
     * @throws ProductException
     * 
     * @return Illuminate\Http\Response
     */
    public function addProduct($cartId, $productId) {
        $this->cartExists($cartId);
        $this->productService->productExists($productId);
        $this->cartProductService->addProduct($cartId, $productId);
        $this->applyOffers($cartId);
        $this->calculateCartTotalAmount($cartId);
        return $this->getCart($cartId);
    }

    /**
     * Removes a product from cart
     *
     * @param int $cartId
     * @param int $productId
     * 
     * @throws CartException
     * 
     * @return Illuminate\Http\Response
     */
    public function removeProduct($cartId, $productId) {
        $this->cartExists($cartId);
        $this->cartProductService->removeProduct($cartId, $productId);
        $this->removeOffers($cartId);
        $this->calculateCartTotalAmount($cartId);
        return $this->getCart($cartId);
    }

    /**
     * Applies offers when add new product to the cart
     *
     * @param int $cartId
     * 
     * 
     */
    private function applyOffers($cartId) {
        $cart = $this->getCart($cartId);
        $cartProducts = $cart->products;
        $productsOffers = $cartProducts->pluck('product')->pluck('offers');
        foreach ($productsOffers as $productOffers) {
            foreach ($productOffers as $offer) {
                $buyProductInCart = $cartProducts->where('product_id', $offer->buy_product_id)->first();
                $getProductInCart = $cartProducts->where('product_id', $offer->get_product_id)->first();
                if (isset($buyProductInCart) && isset($getProductInCart)) {
                    //Products exsit in the cart
                    if ($buyProductInCart->quantity >= $offer->minimum_quantity && $getProductInCart->quantity > 0) {
                        //To get the number of the applied offers for the same product
                        $appliedBuyProductCount = intdiv($buyProductInCart->quantity, $offer->minimum_quantity); //Divides two numbers without remainder
                        $appliedOffersCount = $appliedBuyProductCount <= $getProductInCart->quantity ? $appliedBuyProductCount : $getProductInCart->quantity;
                        $this->cartProductService->updateCartProduct($cartId, $getProductInCart->product_id, $offer->id, $appliedOffersCount, $offer->discount_percentage);
                    }
                }
            }
        }
    }

    /**
     * Removes offers when remove product from the cart
     *
     * @param int $cartId
     * 
     * 
     */
    private function removeOffers($cartId) {
        $cart = $this->getCart($cartId);
        $cartProducts = $cart->products;
        $appliedOffers = $cart->appliedOffers;
        foreach ($appliedOffers as $appliedOffer) {
            $offer = $appliedOffer->offer;
            $buyProductInCart = $cartProducts->where('product_id', $offer->buy_product_id)->first();
            $getProductInCart = $cartProducts->where('product_id', $offer->get_product_id)->first();
            if (is_null($buyProductInCart) || is_null($getProductInCart)) {
                //Remove offer
                $this->cartProductService->removeOffer($cartId, $getProductInCart->product_id, $appliedOffer->id);
            } else {
                if ($buyProductInCart->quantity < $offer->minimum_quantity) {
                    //Remove offer
                    $this->cartProductService->removeOffer($cartId, $getProductInCart->product_id, $appliedOffer->id);
                } else {
                    //To get the number of the applied offers for the same product
                    $appliedBuyProductCount = intdiv($buyProductInCart->quantity, $offer->minimum_quantity); //Divides two numbers without remainder
                    $appliedOffersCount = $appliedBuyProductCount <= $getProductInCart->quantity ? $appliedBuyProductCount : $getProductInCart->quantity;
                    $this->cartProductService->updateCartProduct($cartId, $getProductInCart->product_id, $offer->id, $appliedOffersCount, $offer->discount_percentage);
                }
            }
        }
    }

    /**
     * Calculates the cart sub total and texes
     *
     * @param int $cartId
     * 
     * 
     * @return Illuminate\Http\Response
     */
    private function calculateCartTotalAmount($cartId) {
        $cart = $this->getCart($cartId);
        $subTotal = array_sum($cart->products->pluck('total')->toArray());
        $taxesPercentage = \Config::get('general-config.taxesPpercentage');
        $taxes = $subTotal * $taxesPercentage / 100;
        $total = $subTotal + $taxes;
        $this->cartRepository->updateCart($cartId, $subTotal, $taxesPercentage, $taxes, $total);
    }

}
