<?php

/**
 * Business logic for all offer related operations
 *
 * @author Abanoub Samaan
 */

namespace App\Services;

use App\Constants\Error;
use App\Exceptions\OfferException;
use App\Repositories\OfferRepository;

class OfferService {

    /**
     * Constructor
     *
     * @param OfferRepository $offerRepository
     * 
     */
    public function __construct(OfferRepository $offerRepository) {
        $this->offerRepository = $offerRepository;
    }

    /** @var OfferRepository $offerRepository */
    private $offerRepository;

    /**
     * Gets a specific offer by it's id
     *
     * @param int $offerId
     * 
     * @return Offer
     * @throws OfferException
     */
    public function getOffer($offerId) {
        $offer = $this->offerRepository->getOffer($offerId);
        if (is_null($offer)) {
            throw new OfferException(Error::OFFER_NOT_FOUND);
        }
        return $offer;
    }

    /**
     * Gets a collection of new items
     *
     * @param int $userId
     * @param int $lastSeenId
     * @return Illuminate\Support\Collection of Item
     */
    public function getOffers() {
        return $this->offerRepository->getOffers();
    }

    /**
     * Check the existance of the given offer id, or throw an exception.
     *
     * @param integer $offerId
     * @return true
     * 
     * @throws OfferException
     */
    public function offerExists($offerId) {
        if (!$this->offerRepository->offerExists($offerId)) {
            throw new OfferException(Error::OFFER_NOT_FOUND);
        }
        return true;
    }

}
