<?php

/**
 * Business logic for all product related operations
 *
 * @author Abanoub Samaan
 */

namespace App\Services;

use App\Constants\Error;
use App\Exceptions\ProductException;
use App\Repositories\ProductRepository;

class ProductService {

    /**
     * Constructor
     *
     * @param ProductRepository $productRepository
     * 
     */
    public function __construct(ProductRepository $productRepository) {
        $this->productRepository = $productRepository;
    }

    /** @var ProductRepository $productRepository */
    private $productRepository;

    /**
     * Gets a specific product by it's id
     *
     * @param int $productId
     * 
     * @return Product
     * @throws ProductException
     */
    public function getProduct($productId) {
        $product = $this->productRepository->getProduct($productId);
        if (is_null($product)) {
            throw new ProductException(Error::PRODUCT_NOT_FOUND);
        }
        return $product;
    }

    /**
     * Gets a collection of new items
     *
     * @param int $userId
     * @param int $lastSeenId
     * @return Illuminate\Support\Collection of Item
     */
    public function getProducts() {
        return $this->productRepository->getProducts();
    }

    /**
     * Check the existance of the given product id, or throw an exception.
     *
     * @param integer $productId
     * @return true
     * 
     * @throws ProductException
     */
    public function productExists($productId) {
        if (!$this->productRepository->productExists($productId)) {
            throw new ProductException(Error::PRODUCT_NOT_FOUND);
        }
        return true;
    }

}
