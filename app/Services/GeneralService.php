<?php

/**
 * A service with the sole purpose of handling API responses and other misc supporting tasks
 * through out the application
 *
 * @author Abanoub Samaan
 */

namespace App\Services;

use App\Constants\Error;

class GeneralService {

    /**
     * Gets a response object with success status and the data returned to clients
     * 
     * @param mixed $data
     * 
     * @return Illuminate\Http\Response
     */
    public function getSuccessResponse($data = "") {
        $result = array('status' => 1, 'data' => $data);
        return response()->json($result);
    }

    /**
     * Gets an array of arrays of error codes and their corresponding messages
     * 
     * @param array $errorIdArr
     * 
     * @return array
     */
    public function getErrorArray($errorIdArr) {
        $errorArr = array();
        foreach ($errorIdArr as $errorId) {
            $error = array('code' => $errorId, 'msg' => Error::MSG[$errorId]);
            array_push($errorArr, $error);
        }
        return $errorArr;
    }

    /**
     * Gets a response object for any error that occurs through out the application
     * 
     * @param array $errorIdArr
     * @param int $responseCode
     * 
     * @return Illuminate\Http\Response
     */
    public function getErrorResponse($errorIdArr, $responseCode = Response::HTTP_OK, $causedBy = null) {
        $errorArr = $this->getErrorArray($errorIdArr);
        $result = array('status' => 0, 'errors' => $errorArr);
        if (isset($causedBy)) {
            $result['coused_by'] = $causedBy;
        }
        return response()->json($result, $responseCode);
    }

    /**
     * Gets api response object
     * 
     * @param mixed $data
     * 
     * @return Illuminate\Http\Response
     */
    public function getApiGenericResponse($data) {
        return response()->json($data);
    }

    /**
     * Checks if a user is authenticated
     * 
     * @param type $userId
     * 
     * @return boolean
     * 
     * @throws UnauthorizedUserException
     */
    public function isAuthed($userId) {
        if (Auth::user()->id !== intval($userId)) {
            throw new UnauthorizedUserException();
        }
        return true;
    }

    /**
     * format number
     *
     * @param  $number
     * 
     * @return string
     */
    public function formatNumber($number) {
        if (is_null($number) || floatval($number) === 0.0) {
            return '';
        } elseif (floatval($number) < 0.0) {
            return '$(' . number_format(abs($number), 2, '.', '') . ')';
        } else {
            return '$' . number_format($number, 2, '.', '');
        }
    }

}
