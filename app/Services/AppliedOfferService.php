<?php

/**
 * Business logic for applied offers
 * 
 * @author Abanoub Samaan
 */

namespace App\Services;

use App\Models\AppliedOffer;
use App\Repositories\AppliedOfferRepository;

class AppliedOfferService {

    /**
     * Constructor
     *
     * @param AppliedOfferRepository $appliedOfferRepository
     * 
     */
    public function __construct(AppliedOfferRepository $appliedOfferRepository) {
        $this->appliedOfferRepository = $appliedOfferRepository;
    }

    /** @var AppliedOfferRepository $appliedOfferRepository */
    private $appliedOfferRepository;
    
    /**
     * Add applied offer
     *
     * @param int $cartProductId
     * @param int $offerId
     * @param int $quantity
     * @param float $totalDiscount
     * 
     * @return boolean
     */
    public function addAppliedOffer($cartProductId, $offerId, $quantity, $totalDiscount) {
        $this->appliedOfferRepository->addAppliedOffer($cartProductId, $offerId, $quantity, $totalDiscount);
    }
    
    /**
     * Update applied offer
     *
     * @param int $appliedOfferId
     * @param int $quantity
     * @param float $totalDiscount
     * 
     * @return boolean
     */
    public function updateAppliedOffer($appliedOfferId, $quantity, $totalDiscount) {
        $this->appliedOfferRepository->updateAppliedOffer($appliedOfferId, $quantity, $totalDiscount);
    }
    
    /**
     * Delete applied offer
     *
     * @param int $appliedOfferId
     * 
     * @return boolean
     */
    public function deleteAppliedOffer($appliedOfferId) {
        $this->appliedOfferRepository->deleteAppliedOffer($appliedOfferId);
    }

}
