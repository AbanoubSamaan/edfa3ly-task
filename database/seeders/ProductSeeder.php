<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class ProductSeeder extends Seeder {

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {
        $products = array(
            ['name' => 'T-Shirt', 'price' => 10.99, 'created_at' => now(), 'updated_at' => now()],
            ['name' => 'Pants', 'price' => 14.99, 'created_at' => now(), 'updated_at' => now()],
            ['name' => 'Jacket', 'price' => 19.99, 'created_at' => now(), 'updated_at' => now()],
            ['name' => 'Shoes', 'price' => 24.99, 'created_at' => now(), 'updated_at' => now()]
        );
        array_walk($products, function($product) {
            \DB::table('product')->insert($product);
        });
    }

}
