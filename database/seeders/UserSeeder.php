<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class UserSeeder extends Seeder {

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {
        $users = array(
            ['name' => 'Generic user',
                'email' =>
                'generic@edfa3ly.com',
                'password' => '$2y$12$vwnLs2ufGuxtTmw0ngk12u9I0sdUNOKkzUZXlXZI8rIWrMBd.ei92',
                'created_at' => now(),
                'updated_at' => now()],
        );

        array_walk($users, function($user) {
            \DB::table('user')->insert($user);
        });
    }

}
