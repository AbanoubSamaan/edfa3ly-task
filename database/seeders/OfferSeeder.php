<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class OfferSeeder extends Seeder {

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {
        $offers = array(
            ['description' => 'Shoes are on 10% off.',
                'buy_product_id' => 4,
                'minimum_quantity' => 1,
                'get_product_id' => 4,
                'discount_percentage' => 10,
                'created_at' => now(),
                'updated_at' => now()],
            ['description' => 'Buy two t-shirts and get a jacket half its price.',
                'buy_product_id' => 1,
                'minimum_quantity' => 2,
                'get_product_id' => 3,
                'discount_percentage' => 50,
                'created_at' => now(),
                'updated_at' => now()],
        );

        array_walk($offers, function($cart) {
            \DB::table('offer')->insert($cart);
        });
    }

}
