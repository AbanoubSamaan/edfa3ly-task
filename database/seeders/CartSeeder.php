<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class CartSeeder extends Seeder {

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {
        $carts = array(
            ['user_id' => 1,
                'subtotal' => 0.00,
                'taxes_percentage' => \Config::get('general-config.taxesPpercentage'),
                'taxes' => 0.00,
                'total' => 0.00,
                'created_at' => now(),
                'updated_at' => now()],
        );

        array_walk($carts, function($cart) {
            \DB::table('cart')->insert($cart);
        });
    }

}
