<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCartTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('cart', function (Blueprint $table) {
            $table->id();

            $table->foreignId('user_id')
                    ->constrained('user');

            $table->float('subtotal')->comment('in USD');
            $table->float('taxes_percentage');
            $table->float('taxes')->comment('in USD');
            $table->float('total')->comment('in USD');
            
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::dropIfExists('cart');
    }

}