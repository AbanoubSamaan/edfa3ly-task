<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAppliedOfferTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('applied_offer', function (Blueprint $table) {
            $table->id();

            $table->foreignId('cart_product_id')
                    ->constrained('cart_product');

            $table->foreignId('offer_id')
                    ->constrained('offer');

            $table->integer('quantity');

            $table->float('total_discount')->comment('in USD');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::dropIfExists('applied_offer');
    }

}
