<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOfferTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('offer', function (Blueprint $table) {
            $table->id();

            $table->string('description');

            $table->foreignId('buy_product_id')
                    ->constrained('product');

            $table->integer('minimum_quantity');

            $table->foreignId('get_product_id')
                    ->nullable()
                    ->constrained('product');

            $table->float('discount_percentage');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::dropIfExists('offer');
    }

}
