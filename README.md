# Edfa3ly-Task By Abanoub Samaan

I used the laravel as a framework and mysql as a database.

This project focuses on back-end using REST api standards.

# Installation

In the mysql please add a empty database with the name edfa3ly

Clone the project.

Make a copy of the `.env.example` file and rename it to `.env` inside your project root.

Make sure you add the right database configurations in the .env file.

``` bash
$ composer install
```

Then start the server:

``` bash
$ php artisan serve
```

To import the database testing data:

I have create a migrations and seeders to import some testing data into the database
Just run this command to import data

``` bash
$ bash startup.sh
```

Now everything are working correctly.

# How to test it

I uploaded a postman collection with the environment so you can test the application easily.

#Rest Api documentations

All the apis return the same response structure

Success Response:
```
{
    "status": 1,
    "data": {
        
    }
}
```
Failure Response:
```
{
    "status": 0,
    "errors": [
        {
            "code": error_code_id,
            "msg": error_message
        }
    ]
}
```
Unexpected error will returns a failure response with the caused by message to know where the error is.

#Get all available products

``` 
GET: {{url}}/api/v1/products
```

```
{
    "status": 1,
    "data": {
        "current_page": 1,
        "data": [
            {
                "id": 1,
                "name": "T-Shirt",
                "price": 10.99,
                "offers": [
                    {
                        "id": 2,
                        "description": "Buy two t-shirts and get a jacket half its price.",
                        "discount_percentage": 50,
                        "get_product": {
                            "id": 3,
                            "name": "Jacket",
                            "price": 19.99
                        }
                    }
                ]
            }, 
            .
            .
            .
            .
}
```

#Get a product

``` 
GET: {{url}}/api/v1/products/{{product_id}}
```

```
{
    "status": 1,
    "data": {
        "id": 1,
        "name": "T-Shirt",
        "price": 10.99,
        "offers": [
            {
                "id": 2,
                "description": "Buy two t-shirts and get a jacket half its price.",
                "discount_percentage": 50,
                "get_product": {
                    "id": 3,
                    "name": "Jacket",
                    "price": 19.99
                }
            }
        ]
    }
}
```

#Get a cart

``` 
GET: {{url}}/api/v1/cart/{{cart_id}}
```

```
{
    "status": 1,
    "data": {
        "id": 1,
        "user_id": 1,
        "subtotal": 22.49,
        "taxes_percentage": 14,
        "taxes": 3.15,
        "total": 25.64,
        "products": [
            {
                "quantity": 1,
                "total": 22.49,
                "product": {
                    "id": 4,
                    "name": "Shoes",
                    "price": 24.99
                }
            }
        ],
        "applied_offers": [
            {
                "quantity": 1,
                "total_discount": 2.5,
                "laravel_through_key": 1,
                "offer": {
                    "id": 1,
                    "description": "Shoes are on 10% off.",
                    "discount_percentage": 10
                }
            }
        ]
    }
}
```

#Get offers

``` 
GET: {{url}}/api/v1/offers
```

```
{
    "status": 1,
    "data": {
        "current_page": 1,
        "data": [
            {
                "id": 1,
                "description": "Shoes are on 10% off.",
                "discount_percentage": 10,
                "buy_product": {
                    "id": 4,
                    "name": "Shoes",
                    "price": 24.99
                },
                "get_product": {
                    "id": 4,
                    "name": "Shoes",
                    "price": 24.99
                }
            },
            .
            .
            .
```
#Get an offer

``` 
GET: {{url}}/api/v1/offers/{{offer_id}}
```

```
{
    "status": 1,
    "data": {
        "id": 1,
        "description": "Shoes are on 10% off.",
        "discount_percentage": 10,
        "buy_product": {
            "id": 4,
            "name": "Shoes",
            "price": 24.99
        },
        "get_product": {
            "id": 4,
            "name": "Shoes",
            "price": 24.99
        }
    }
}
```

#Add product to cart

``` 
POST: {{url}}/api/v1/cart/{{cart_id}}/products/actions/add-product
```

#Remove product from cart

``` 
POST: {{url}}/api/v1/cart/{{cart_id}}/products/actions/remove-product
```