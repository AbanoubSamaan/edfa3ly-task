echo 'Start Creating Database Tables'
php artisan migrate:refresh
php artisan migrate --force
echo 'Start Insertig Data'
php artisan db:seed --class=UserSeeder
php artisan db:seed --class=ProductSeeder
php artisan db:seed --class=CartSeeder
php artisan db:seed --class=OfferSeeder
echo 'Done'

