<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\ApiController;
use App\Http\Controllers\ProductController;
use App\Http\Controllers\CartController;
use App\Http\Controllers\OfferController;

/*
  |--------------------------------------------------------------------------
  | API Routes
  |--------------------------------------------------------------------------
  |
  | Here is where you can register API routes for your application. These
  | routes are loaded by the RouteServiceProvider within a group which
  | is assigned the "api" middleware group. Enjoy building your API!
  |
 */

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});


Route::group(['prefix' => 'v1'], function () {
    Route::get('/', [ApiController::class, 'getApiInformation']);

    Route::group(['prefix' => 'products'], function () {
        Route::get('/', [ProductController::class, 'getProducts']);

        Route::get('{productId}', [ProductController::class, 'getProduct'])->where('productId', '[\-0-9]+');
    });

    Route::group(['prefix' => 'carts'], function () {
        Route::group(['prefix' => '{cartId}'], function () {
            Route::get('/', [CartController::class, 'getCart'])->where('cartId', '[\-0-9]+');

            Route::group(['prefix' => 'products'], function () {
                Route::get('/', [CartController::class, 'getCartProducts']);

                Route::group(['prefix' => 'actions'], function () {

                    Route::post('add-product', [CartController::class, 'addProduct']);

                    Route::post('remove-product', [CartController::class, 'removeProduct']);
                });
            });
        });
    });

    Route::group(['prefix' => 'offers'], function () {
        Route::get('/', [OfferController::class, 'getOffers']);

        Route::get('{offerId}', [OfferController::class, 'getOffer'])->where('offerId', '[\-0-9]+');
    });
});
